from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
import os
app = Flask(__name__)
POSTGRES_USER = os.environ.get('POSTGRES_USER')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
POSTGRES_DB = os.environ.get('POSTGRES_DB')
POSTGRES_SERVER = os.environ.get('POSTGRES_SERVER')
# app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://postgres:postgres@192.168.1.7:5432/photo'
#app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///shop.db"
app.config["SQLALCHEMY_DATABASE_URI"] = f'postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}:5432/{POSTGRES_DB}'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)


# DATABASE_URL = f'postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@postgres:5432/books'


class Item(db.Model):
    #__tablename__ = 'item'
    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.Integer(), nullable=False)
    price = db.Column(db.Integer(), nullable=False)
  #  isActive = db.Column(db.Boolean, default=True)
    #text = db.Column(db.Text, nullable=False)

    def __init__(self, title, price):
        self.title=title
        self.price=price
    # def __repr__(self):
    #     return self.title

with app.app_context():
    db.create_all()

@app.route('/create', methods = [ "POST","GET"])
def create():
    if request.method == "POST":
        title = request.form['title']
        price = request.form['price']
        print(title, price)
        item = Item(title, price)
        try:
            print(item)
            print(title, price)
            db.session.add(item)
            db.session.commit()
            return redirect('/')
        except:
            return "Получилась ошибка"
    else:
        return(render_template('create.html'))

@app.route('/')
def index():
    items = Item.query.order_by(Item.price).all()
    return render_template('index.html', data=items)

@app.route('/about')
def about():
    return(render_template('about.html'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug = True)
