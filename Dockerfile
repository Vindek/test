FROM python:3.8-slim AS build
ENV PYTHONUNBUFFERED=1
RUN apt-get update && apt-get install -y build-essential libpq-dev && rm -rf /var/lib/apt/lists/*  
COPY ./magazin/requirements.txt /code/magazin/
WORKDIR /code/magazin
RUN pip3 install -r requirements.txt

FROM python:3.8-slim  
COPY --from=build /usr/local/lib/python3.8/site-packages/ /usr/local/lib/python3.8/site-packages/ 
COPY --from=build /usr/lib/x86_64-linux-gnu/ /usr/lib/x86_64-linux-gnu/
COPY ./magazin /code/magazin
WORKDIR /code/magazin
CMD python3 index.py


